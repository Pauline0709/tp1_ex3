class simple_calculator:
    def sum(self, numero_1, numero_2):
        self.result = numero_1 + numero_2
        return self.result

    def substract(self, numero_3, numero_4):
        self.result = numero_3 - numero_4
        return self.result

    def multiply(self, numero_5, numero_6):
        self.result = numero_5 * numero_6
        return self.result

    def divide(self, numero_7, numero_8):
        self.result = numero_7 / numero_8
        return self.result

num_1 = 5
num_2 = 7
s = simple_calculator()
print(s.sum(num_1, num_2))
print(s.substract(num_1, num_2))
print(s.multiply(num_1, num_2))
print(s.divide(num_1, num_2))
